# Impossible Tic-Tac-Toe Engine

This is a game engine that plays the game tic-tac-toe, and is unbeatable.
The engine employs an algorithm that chooses the first available move from the following list:
https://en.wikipedia.org/wiki/Tic-tac-toe#Strategy. With this algorithm, 
you will be able to play a perfect game of tic-tac-toe (ending with a win or at least a draw).

## Installation

* In terminal, navigate to the folder you want to clone the code repository to.
* Clone the repository from Gitlab. 

        cd example/folder/code
        git clone https://gitlab.com/sean_yi/impossible-tic-tac-toe-engine.git

* In addition, please download the most recent version of Python 
  from https://www.python.org/downloads/ (At least version 3.8).
  
## Usage

* In terminal, navigate to the folder where the code repo is.
* Run the python script using **Python 3.8**.

        cd example/ImpossibleTTT
        python3 impossibleTTT.py