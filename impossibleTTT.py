import time


# GameBoard object
class GameBoard:
    # Creates board itself
    def __init__(self):
        self.board = [[" " if column % 2 == 0 else "|" for column in range(5)] for row in range(3)]

    # Add an X or O to game board
    def add(self, current, row, column):
        # depending on if current = player or engine, add an X or O value here
        if current == "Player" or current == "O":
            self.board[row][column] = "O"
        elif current == "Engine" or current == "X":
            self.board[row][column] = "X"
        else:
            self.board[row][column] = " "

    # Overloading print function for game board, so it prints in a pretty manner
    def __str__(self):
        board = ""
        for row in range(len(self.board)):
            print("")
            for column in range(len(self.board[row])):
                print(self.board[row][column], end=""),

        print("")
        return board


# Intro explaining program and rules
def rule_intro(beginning_board):
    print("Welcome to the Impossible Tic-Tac-Toe Engine!")
    print("This is a simple program that will never lose a game of Tic-Tac-Toe.")
    print("I challenge you to beat it!")
    print("")
    input("To begin the game, press enter...")
    print("")
    print("Here is what the game board looks like:")
    print(beginning_board)


# Player's turn, asking for input for row and column, and checking if it's a valid place to play
def player_turn(current_board):
    print("Your turn!")

    while True:
        try:
            row = int(input("Please input the row # you'd like to play from 1-3: "))
            if 0 < row < 4:
                row -= 1  # readjusts row placement for array values
                break
            else:
                print("Invalid number.")

        except Exception:
            print("Invalid input.")

    while True:
        try:
            column = int(input("Please input the column # you'd like to play from 1-3: "))
            if 0 < column < 4:
                # readjust column placement for array values
                if column == 1:
                    column -= 1
                elif column == 3:
                    column += 1

                if current_board.board[row][column] == " ":
                    break
                else:
                    print("That space is already taken.")
            else:
                print("Invalid number.")

        except Exception:
            print("Invalid input.")

    current_board.add("Player", row, column)
    print(current_board)
    return current_board


# Engine's turn, calculating the best option from the 9 valid moves
def engine_turn(current_board):
    print("Engine's turn!")
    print("Calculating...")
    time.sleep(2)

    # algorithms here to calculate which out of the 9 valid moves to choose
    if check_two_row(current_board, "X", "O"):  # win
        row, column = fill_third(current_board, "X")

    elif check_two_row(current_board, "O", "X"):  # block
        row, column = fill_third(current_board, "O")

    elif rcDict := count_fork(current_board, "X", "O"):  # fork
        row = rcDict["row"]
        column = rcDict["column"]

    elif rcDict := count_fork(current_board, "O", "X"):  # fork block
        row = rcDict["row"]
        column = rcDict["column"]

    elif current_board.board[1][2] == " ":  # center
        row = 1
        column = 2

    elif rcDict := find_corner(current_board, "O"):  # opposite corner
        row = rcDict["row"]
        column = rcDict["column"]

    elif rcDict := find_corner(current_board, " "):  # empty corner
        row = rcDict["row"]
        column = rcDict["column"]

    else:  # empty side
        row, column = find_side(current_board)

    current_board.add("Engine", row, column)
    print(current_board)
    return current_board


# Check if there is a row of two
def check_two_row(current_board, current_player, enemy_player):
    left_right_diag = []
    right_left_diag = []

    for column in range(5):
        if column % 2 == 0:
            temp_column = []

            for row in range(3):
                # horizontal row check
                if current_board.board[row].count(current_player) == 2 and enemy_player not in current_board.board[row]:
                    return True

                # temp list for vertical column check
                temp_column.append(current_board.board[row][column])

                if len(left_right_diag) * 2 == column and len(left_right_diag) == row:
                    # temp list for left to right diagonal
                    left_right_diag.append(current_board.board[row][column])

                    # temp list for right ot left diagonal
                    right_left_diag.append(current_board.board[row][len(current_board.board[row]) - 1 - column])

            # vertical column check
            if temp_column.count(current_player) == 2 and enemy_player not in temp_column:
                return True

    # diagonals check
    if (left_right_diag.count(current_player) == 2 and enemy_player not in left_right_diag) or \
            (right_left_diag.count(current_player) == 2 and enemy_player not in right_left_diag):
        return True

    return False


# Count rows of twos
def count_two_row(current_board, current_player, enemy_player):
    left_right_diag = []
    right_left_diag = []
    num_rows = 0

    for column in range(5):
        if column % 2 == 0:
            temp_column = []

            for row in range(3):
                # horizontal row check
                if current_board.board[row].count(current_player) == 2 \
                        and enemy_player not in current_board.board[row] and column == 0:
                    num_rows += 1

                # temp list for vertical column check
                temp_column.append(current_board.board[row][column])

                if len(left_right_diag) * 2 == column and len(left_right_diag) == row:
                    # temp list for left to right diagonal
                    left_right_diag.append(current_board.board[row][column])

                    # temp list for right ot left diagonal
                    right_left_diag.append(current_board.board[row][len(current_board.board[row]) - 1 - column])

            # vertical column check
            if temp_column.count(current_player) == 2 and enemy_player not in temp_column:
                num_rows += 1

    # diagonals check
    if left_right_diag.count(current_player) == 2 and enemy_player not in left_right_diag:
        num_rows += 1
    elif right_left_diag.count(current_player) == 2 and enemy_player not in right_left_diag:
        num_rows += 1

    return num_rows


# Check if there is a row of three
def check_three_row(current_board, current_player):
    left_right_diag = []
    right_left_diag = []

    for column in range(5):
        if column % 2 == 0:
            temp_column = []

            for row in range(3):
                # horizontal row check
                if current_board.board[row].count(current_player) == 3:
                    return True

                # temp list for vertical column check
                temp_column.append(current_board.board[row][column])

                if len(left_right_diag) * 2 == column and len(left_right_diag) == row:
                    # temp list for left to right diagonal
                    left_right_diag.append(current_board.board[row][column])

                    # temp list for right ot left diagonal
                    right_left_diag.append(current_board.board[row][len(current_board.board[row]) - 1 - column])

            # vertical column check
            if temp_column.count(current_player) == 3:
                return True

    # diagonals check
    if left_right_diag.count(current_player) == 3 or right_left_diag.count(current_player) == 3:
        return True

    return False


# Find spot to fill in to make three in a row
def fill_third(current_board, current_player):
    for row in range(len(current_board.board)):
        for column in range(len(current_board.board[row])):
            if current_board.board[row][column] == " ":
                current_board.add(current_player, row, column)
                if check_three_row(current_board, current_player):
                    return row, column
                else:
                    current_board.add("Blank", row, column)


# See if there is a potential fork to create or block
def count_fork(current_board, current_player, enemy_player):
    return_dict = {}

    for row in range(len(current_board.board)):
        for column in range(len(current_board.board[row])):
            if current_board.board[row][column] == " ":
                current_board.add(current_player, row, column)
                num_rows = count_two_row(current_board, current_player, enemy_player)
                if num_rows >= 2:
                    return_dict["row"] = row
                    return_dict["column"] = column
                    return return_dict
                else:
                    current_board.add("Blank", row, column)

    return return_dict


# Find a corner to fill or block
def find_corner(current_board, current_player):
    corners = [[0, 0], [0, 4], [2, 0], [2, 4]]
    return_dict = {}

    for corner in range(len(corners)):
        if current_board.board[corners[corner][0]][corners[corner][1]] == current_player \
                and current_player == " ":
            return_dict["row"] = corners[corner][0]
            return_dict["column"] = corners[corner][1]
            return return_dict
        elif current_board.board[corners[corner][0]][corners[corner][1]] == current_player:
            if corner + 2 >= len(corners):
                corner -= 2
            return_dict["row"] = corners[corner][0]
            return_dict["column"] = corners[corner][1]
            return return_dict

    return return_dict


# Find a side to fill or block
def find_side(current_board):
    sides = [[0, 2], [1, 0], [1, 4], [2, 2]]

    for side in sides:
        if current_board.board[side[0]][side[1]] == " ":
            return side[0], side[1]

    return 0, 0  # This should never happen


# Checking if a row of 3 has been made on the gameboard, OR if all playable spaces have been filled
def game_over(current_board, current_player):
    if board_filled(current_board) or check_three_row(current_board, current_player):
        return False
    else:
        return True


# Check if all spots have been filled
def board_filled(current_board):
    num_spots = 9
    for row in range(3):
        num_spots -= current_board.board[row].count(" ")

    return num_spots == 9


# Wrap up game, show results, and ask if player would like to play again
def show_results(final_board):
    print("Game over!!")
    print("Here is the final board!")
    print(final_board)


# Main method giving workflow of the game
def main():
    game_board = GameBoard()
    game_status = True
    rule_intro(game_board)

    # Actual gameplay
    while game_status:
        game_board = player_turn(game_board)
        game_status = game_over(game_board, "O")

        game_board = engine_turn(game_board)
        game_status = game_over(game_board, "X")

    show_results(game_board)

    # Asking to play again
    while True:
        response = input("Would you like to play again? (Y/N): ")
        if response in ("Y", 'y'):
            print()
            main()
        else:
            print("Thank you so much for playing my game!")
            break


# Program start
if __name__ == '__main__':
    main()
